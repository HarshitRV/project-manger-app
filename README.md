# Project Manager App

This is a React application that uses features such as React Router, Context API, and Hooks (useState, useContext, useRef, useReducer).

## Setup

To set up this project locally, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Run `npm install` to install the project dependencies.
4. Run `npm run dev` to start the development server.

## Features

This application uses several React features:

- React Router: Used for routing in the application. Check out the routing setup in `src/main.jsx`.
- Context API: Used for state management across the application. The context is provided in `src/context/ProjectsProvider.jsx` and consumed in various components like `src/components/tasks/NewTask.jsx`.
- Hooks: This application uses several React Hooks:
    - useState: Used for local state management in components like `src/components/tasks/NewTask.jsx`.
    - useContext: Used to consume the context in components.
    - useRef: Used to manage refs in components like `src/components/tasks/NewTask.jsx`.
    - useReducer: Used in `src/routes/Root.jsx` for state management.

## Contributing

This project is licensed under the terms of the MIT license.