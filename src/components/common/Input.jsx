import { forwardRef } from "react";

function InputComponent(
	{ htmlFor = "", inputType = "text", isTextArea = false, label = "" },
	ref
) {
	const classes =
		"w-full p-1 border-p-2 rounded-sm border-stone-300 bg-stone-200 text-stone-600 focus:outline-none focus:border-stone-600";
	return (
		<p className="flex flex-col gap-1 my-4">
			<label htmlFor="title">{label}</label>
			{isTextArea ? (
				<textarea
					ref={ref}
					className={classes}
					name={htmlFor}
					id={htmlFor}
					cols="30"
					rows="10"></textarea>
			) : (
				<input
					ref={ref}
					className={classes}
					name={htmlFor}
					id={htmlFor}
					type={inputType}
				/>
			)}
		</p>
	);
}

const Input = forwardRef(InputComponent);

export default Input;
