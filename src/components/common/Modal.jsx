import { createPortal } from "react-dom";
import { forwardRef, useImperativeHandle, useRef } from "react";

function ModalComponent({ children, buttonCaption = "Close" }, ref) {
	const dialogRef = useRef();

	useImperativeHandle(ref, () => {
		return {
			open() {
				dialogRef.current.showModal();
			},
		};
	});

	return createPortal(
		<dialog
			ref={dialogRef}
			className="backdrop:bg-stone-900/90 p-4 rounded-md shadow-md">
			{children}

			<div className="mt-4 text-right">
				<button
					className="px-4 py-2 text-xs md:text-base rounded-md bg-stone-700 text-stone-400 hover:bg-stone-600 hover:text-stone-100"
					onClick={() => {
						dialogRef.current.close();
					}}>
					{buttonCaption}
				</button>
			</div>
		</dialog>,
		document.getElementById("modal-root")
	);
}

const Modal = forwardRef(ModalComponent);

export default Modal;
