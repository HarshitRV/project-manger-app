import { useContext, useState, useRef } from "react";
import { useParams } from "react-router";
import { ProjectDispatchContext } from "../../context/ProjectsContext";
import Modal from "../common/Modal";

export default function NewTask() {
	const modalRef = useRef(null);
	const { projectId } = useParams();
	const dispatch = useContext(ProjectDispatchContext);

	const [taskName, setTaskName] = useState("");
	const taskNameChangeHandler = (event) => {
		setTaskName(event.target.value);
	};

	const addTaskHandler = async () => {
		console.log("adding task with name: ", taskName);
		if (taskName.trim() === "") {
			modalRef.current.open();
			return;
		}
		await dispatch({
			type: "addTask",
			projectId,
			taskName,
		});
		setTaskName("");
	};

	return (
		<>
			<Modal ref={modalRef}>
				<h2 className="text-xl font-bold text-stone-500 mt-4 my-4">
					Invalid Input
				</h2>
				<p className="text-stone-400 mb-4">
					Oops...looks like you forgot to enter a value
				</p>
				<p className="text-stone-400 mb-4">
					Make sure to provide value for the task
				</p>
			</Modal>
			<div className="flex items-center gap-4">
				<input
					className="w-64 px-2 py-1 rounded-sm bg-stone-200"
					type="text"
					placeholder="Task Name"
					name="task-name"
					value={taskName}
					onChange={taskNameChangeHandler}
				/>
				<button
					onClick={addTaskHandler}
					className="text-stone-700 hover:text-stone-950">
					Add Task
				</button>
			</div>
		</>
	);
}
