import { useContext } from "react";
import NewTask from "./NewTask";
import { useParams } from "react-router";
import {
	ProjectDispatchContext,
	ProjectsContext,
} from "../../context/ProjectsContext";

export default function Tasks() {
	const { projectId } = useParams();

	const projects = useContext(ProjectsContext);
	const dispatch = useContext(ProjectDispatchContext);
	const project = projects.find((project) => project.id === projectId);

	const handleTaskClear = (index) => {
		dispatch({
			type: "clearTask",
			index,
			projectId,
		});
	};

	return (
		<section>
			<h2 className="text-2xl font-bold text-stone-700 mb-4">Tasks</h2>
			<NewTask />
			{project.tasks.length === 0 ? (
				<p className="text-stone-800 my-4">
					This project does not have any tasks yet
				</p>
			) : (
				<ul className="mt-8 rounded-md">
					{project.tasks.map((task, index) => (
						<li
							className="flex justify-between p-2 my-4 rounded-md bg-stone-100"
							key={index}>
							<span>{task}</span>
							<button
								onClick={() => handleTaskClear(index)}
								className="text-stone-700 hover:text-red-500">
								Clear
							</button>
						</li>
					))}
				</ul>
			)}
		</section>
	);
}
