import { useNavigate } from "react-router";
import {
	ProjectsContext,
	ProjectDispatchContext,
} from "../../context/ProjectsContext";
import { useContext, useEffect } from "react";
import { useParams } from "react-router";
import Tasks from "../tasks/Tasks";


export default function SelectedProject() {
	const navigate = useNavigate();

	const projects = useContext(ProjectsContext);
	const dispatch = useContext(ProjectDispatchContext);

	const { projectId } = useParams();

	const project = projects.find((project) => project.id === projectId);

	console.log("Selected Project", project);

	const handleDeleteProject = async () => {
		await dispatch({
			type: "delete",
			projectId,
		});

		navigate("/");
	};

	const formatDate = (date) => {
		if (date.length > 1) {
			return new Date(date).toLocaleDateString("en-US", {
				year: "numeric",
				month: "long",
				day: "2-digit",
			});
		}

		return "No due date";
	};

	useEffect(() => {
		if (!project) {
			navigate("/");
		}
	}, []);

	return project ? (
		<div className="w-[35rem] mt-16">
			<header className="pb-4 mb-4 border-b-2 border-stone-300">
				<div className="flex items-center justify-between">
					<h1 className="text-3xl font-bold text-stone-300 mb-2">
						{project.title}
					</h1>
					<button
						onClick={handleDeleteProject}
						className="text-stone-600 hover:text-stone-950">
						Delete
					</button>
				</div>
				<p className="mb-4 text-stone-400">{formatDate(project.date)}</p>
				<p className="text-stone-600 whitespace-pre-wrap">
					{project.description}
				</p>
			</header>
			<Tasks />
		</div>
	) : (
		<p>No projects found</p>
	);
}
