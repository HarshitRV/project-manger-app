import { Link, NavLink } from "react-router-dom";
import { useContext } from "react";
import { ProjectsContext } from "../../context/ProjectsContext";

export default function ProjectSidebar() {
	const projects = useContext(ProjectsContext);

	const navlinkClasses =
		"w-full text-left px-2 py-1 rounded-sm text-stone-400 hover:text-stone-200";

	return (
		<aside className="w-1/3 px-8 py-16 bg-stone-900 text-stone-50 md:w-72">
			<h2 className="mb-8 font-bold uppercase md:text-xl text-stone-200">
				Your Projects
			</h2>
			<div>
				<Link
					to="/new-project"
					className="px-4 py-2 text-xs md:text-base rounded-md bg-stone-700 text-stone-400 hover:bg-stone-600 hover:text-stone-100">
					+ Add Project
				</Link>
			</div>
			<ul className="mt-8 flex flex-col">
				{projects.map((project, index) => {
					return (
						<NavLink
							to={"/project/" + project.id}
							key={index}
							className={({ isActive }) =>
								isActive ? `${navlinkClasses} bg-stone-800` : navlinkClasses
							}>
							{project.title}
						</NavLink>
					);
				})}
			</ul>
		</aside>
	);
}
