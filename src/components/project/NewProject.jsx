import { useRef } from "react";
import { Link } from "react-router-dom";
import Input from "../common/Input";
import { useContext } from "react";
import { ProjectDispatchContext } from "../../context/ProjectsContext";
import { useNavigate } from "react-router-dom";
import Modal from "../common/Modal";
import { ProjectsContext } from "../../context/ProjectsContext";
import { v4 as uuidv4 } from "uuid";

export default function NewProject() {
	const dispatch = useContext(ProjectDispatchContext);
	const projects = useContext(ProjectsContext);

	const navigate = useNavigate();

	const modalRef = useRef();

	const titleRef = useRef(null);
	const descriptionRef = useRef(null);
	const dateRef = useRef(null);

	const handleProjectSave = async () => {
		const title = titleRef.current.value;
		const description = descriptionRef.current.value;
		const date = dateRef.current.value;

		const projectInfo = {
			id: uuidv4(),
			title,
			description,
			date,
		};

		if (title.trim().length === 0 || description.trim().length === 0) {
			modalRef.current.open();
			return;
		}

		//! Possible bug -------------------------
		await dispatch({
			type: "save",
			projectInfo,
		});

		navigate("/project/" + projectInfo.id);
		//! --------------------------------------
	};

	return (
		<>
			<Modal ref={modalRef}>
				<h2 className="text-xl font-bold text-stone-500 mt-4 my-4">
					Invalid Input
				</h2>
				<p className="text-stone-400 mb-4">
					Oops...looks like you forgot to enter a value
				</p>
				<p className="text-stone-400 mb-4">
					Make sure to provide value for every value type
				</p>
			</Modal>
			<div className="w-[35rem] mt-16">
				<menu className="flex items-center justify-end gap-4 my-4">
					<li>
						<Link
							to="/"
							className="text-stone-800 hover:text-stone-950">
							Cancel
						</Link>
					</li>
					<li>
						<button
							onClick={handleProjectSave}
							className="px-6 py-2 rounded-md bg-stone-800 text-stone-50 hover:bg-stone-950">
							Save
						</button>
					</li>
				</menu>
				<div>
					<Input
						htmlFor="title"
						label="Title"
						inputType="text"
						ref={titleRef}
					/>
					<Input
						htmlFor="description"
						label="Description"
						isTextArea
						ref={descriptionRef}
					/>
					<Input
						htmlFor="due-date"
						label="Due Date"
						inputType="date"
						ref={dateRef}
					/>
				</div>
			</div>
		</>
	);
}
