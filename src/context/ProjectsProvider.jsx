import { useReducer } from "react";
import {
	ProjectsContext,
	ProjectDispatchContext,
	projectReducer,
} from "./ProjectsContext";

export default function ProjectsProvider({ children }) {
	const [projects, dispatch] = useReducer(projectReducer, []);

	return (
		<ProjectsContext.Provider value={projects}>
			<ProjectDispatchContext.Provider value={dispatch}>
				{children}
			</ProjectDispatchContext.Provider>
		</ProjectsContext.Provider>
	);
}
