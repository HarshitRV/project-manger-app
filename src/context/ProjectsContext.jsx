import { createContext } from "react";

export const ProjectsContext = createContext(null);
export const ProjectDispatchContext = createContext(null);

export class Project {
	constructor(id, title, description, date = "") {
		this.id = id;
		this.title = title;
		this.description = description;
		this.tasks = [];
		this.date = date;
	}

	getProjectInfo() {
		return {
			id: this.id,
			title: this.title,
			description: this.description,
			date: this.date,
			tasks: this.tasks,
		};
	}
}

export const projectReducer = (projects = [], action) => {
	switch (action.type) {
		case "save": {
			const { id, title, description, date = "" } = action.projectInfo;

			const project = new Project(id, title, description, date);

			console.log("SAVED", project);

			return [...projects, project];
		}

		case "delete": {
			console.log("DELETED project with id: ", action.projectId);
			return projects.filter((project) => project.id !== action.projectId);
		}

		case "addTask": {
			const { projectId, taskName } = action;
			console.log(`Added task ${taskName} with project id ${projectId}`);

			return projects.map((project) => {
				if (project.id === projectId) {
					return { ...project, tasks: [...project.tasks, taskName] };
				}
				return { ...project };
			});
		}

		case "clearTask": {
			const { index, projectId } = action;
			return projects.map((project) => {
				if (project.id === projectId) {
					return {
						...project,
						tasks: project.tasks.filter((_, taskIndex) => {
							return taskIndex !== index;
						}),
					};
				}
				return { ...project };
			});
		}

		default: {
			return [...projects];
		}
	}
};
