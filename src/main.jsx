import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { RouterProvider, createHashRouter } from "react-router-dom";
import Root from "./routes/Root";
import NewProject from "./components/project/NewProject";
import NoProjectSelected from "./components/project/NoProjectSelected";
import SelectedProject from "./components/project/SelectedProject";

const router = createHashRouter([
	{
		path: "/",
		element: <Root />,
		children: [
			{ index: true, element: <NoProjectSelected /> },
			{
				path: "/new-project",
				element: <NewProject />,
			},
			{
				path: "/project/:projectId",
				element: <SelectedProject />,
			},
		],
	},
]);

ReactDOM.createRoot(document.getElementById("root")).render(
	<React.StrictMode>
		<RouterProvider router={router} />
	</React.StrictMode>
);
