import { Outlet } from "react-router";
import ProjectSidebar from "./components/project/ProjectSidebar";

export default function Root() {
	return (
		<main className="h-screen flex gap-8">
			<ProjectSidebar />
			<Outlet />
		</main>
	);
}
