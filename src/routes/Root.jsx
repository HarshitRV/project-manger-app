import { useReducer } from "react";
import App from "../App";
import ProjectsProvider from "../context/ProjectsProvider";

export default function Root() {
	return (
		<ProjectsProvider>
			<App />
		</ProjectsProvider>
	);
}
